#!/bin/bash

for version in 1.15; do
    content=$(cat Dockerfile)
    tag="$version-alpine"

    echo "${content/\$IMAGE_TAG/$tag}" > tmp.Dockerfile

    IMAGE_TAG=$CI_REGISTRY_IMAGE/$tag:$CI_BUILD_REF_NAME
    docker build -t $IMAGE_TAG -f tmp.Dockerfile .
    docker push $IMAGE_TAG
done