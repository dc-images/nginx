FROM nginx:$IMAGE_TAG

RUN mkdir -p /before_scripts

COPY ./config/nginx.conf /etc/nginx/nginx.conf
COPY ./config/entrypoint.sh /entrypoint.sh
COPY ./config/before_scripts/ /before_scripts/
COPY ./errors /data/errors

RUN addgroup -g 82 -S www-data \
	&& adduser -u 82 -D -S -G www-data www-data \
	&& mkdir -m 0775 /data || chmod -R 0775 /data \
    && chown -R www-data /data

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
