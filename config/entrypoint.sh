#!/bin/sh

for filename in /before_scripts/*; do
    sh $filename
done

echo "Starting NGINX.";
exec nginx;
