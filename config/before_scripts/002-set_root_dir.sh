#!/bin/sh

[ -z $NGINX_ROOT_DIR ] && NGINX_ROOT_DIR="www"

# Set it as root directory
mkdir -p /data/$NGINX_ROOT_DIR

sed -i -e "s/%root%/$NGINX_ROOT_DIR/g" /etc/nginx/nginx.conf