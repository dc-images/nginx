#!/bin/sh

REPLACE=""
[ -z $FPM_UPSTREAM ] && FPM_UPSTREAM="php-fpm:9000"

if [ ! -z $DISABLE_DNS_CACHE ] && [ $DISABLE_DNS_CACHE="1" ]; then
    if [ -z $RESOLVER ]; then
        echo "You must provide resolver in order to disable DNS cache"
        exit 1
    fi

    echo "Disabling DNS cache"
    REPLACE="resolver $RESOLVER valid=5s ipv6=off;\n            set \$fpm_upstream \"$FPM_UPSTREAM\";\n            fastcgi_pass   \$fpm_upstream;"
else
    REPLACE="fastcgi_pass   $FPM_UPSTREAM;"
fi

echo "Setting fastcgi pass configuration"
sed -i -e "s/%fastcgi_pass%/$REPLACE/g" /etc/nginx/nginx.conf;